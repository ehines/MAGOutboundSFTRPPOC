﻿
using Microsoft.Azure.WebJobs.Host;
using Microsoft.WindowsAzure.Storage.Blob;
using Renci.SshNet;
using System;
using System.IO;

namespace magoutboundsftpfa
{
    class Sftp
    {
        public static void UploadSFTPFile(string host, string username,
        string password, CloudBlockBlob blob, string destinationpath, int port, TraceWriter log)
        {
            try
            {
                log.Info("Beginning SFTP upload.");
                using (SftpClient client = new SftpClient(host, port, username, password))
                {
                    client.Connect();
                    const int mb = 1024;
                    const int cacheSize = 100;
                    client.BufferSize = cacheSize * mb;
                    client.ChangeDirectory(destinationpath);
                    client.UploadFile(blob.OpenRead(), Path.GetFileName(blob.Name));

                }
            } catch (Renci.SshNet.Common.SftpPermissionDeniedException e)
            {
                log.Info("Permission denied to destination SFTP Server.");
                throw (e);
            } catch (Renci.SshNet.Common.SftpPathNotFoundException e)
            {
                log.Info("Destination SFTP Path not found.");
                throw (e);
            } catch (Exception e)
            {
                log.Info("Unhandled exception encountered.");
                throw (e);
            }
        }
    }
}
