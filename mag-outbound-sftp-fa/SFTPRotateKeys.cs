using System;
using System.Configuration;
using System.Threading.Tasks;
using Microsoft.Azure.KeyVault;
using Microsoft.Azure.KeyVault.Models;
using Microsoft.Azure.Services.AppAuthentication;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;

namespace magoutboundsftpfa
{

public static class SFTPRotateKeys
    {

        public static SecretBundle GetKeyVaultValueFromSecret(KeyVaultClient kv, string secretIdentifier)
        {
            var keyVaultName = ConfigurationManager.AppSettings["KeyVaultName"];
            var sasToken = (kv.GetSecretAsync($"https://{keyVaultName}.vault.azure.net/secrets/{secretIdentifier}"));

            Task.WaitAll(sasToken);


            return sasToken.Result;
        }

        [FunctionName("SFTPRotateKeys")]
        public static void Run([TimerTrigger("0 /1 * * *")]TimerInfo myTimer, TraceWriter log)
        {
        log.Info($"Regenerating keys for OutboundSFTP at: {DateTime.Now}");
        var storageAccountName = ConfigurationManager.AppSettings["StorageAccountName"];
        var keyVaultName = ConfigurationManager.AppSettings["KeyVaultName"];

        AzureServiceTokenProvider azureTokenProvider = new AzureServiceTokenProvider();
        KeyVaultClient kv = new KeyVaultClient(new KeyVaultClient.AuthenticationCallback(azureTokenProvider.KeyVaultTokenCallback));


        kv.RegenerateStorageAccountKeyAsync($"https://{keyVaultName}.vault.azure.net/", storageAccountName, "key2");

        var sasToken = GetKeyVaultValueFromSecret(kv, $"{storageAccountName}-{ConfigurationManager.AppSettings["FullAccessSecretSuffix"]}");

         kv.SetSecretAsync($"https://{keyVaultName}.vault.azure.net/", $"{storageAccountName}-BlobSasURL", $"https://{storageAccountName}.blob.core.windows.net/mag-outbound-sftp-blob-container?{sasToken}");
    }
    }
}
