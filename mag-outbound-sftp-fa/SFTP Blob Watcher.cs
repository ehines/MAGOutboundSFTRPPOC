using System;
using System.Configuration;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Azure.KeyVault;
using Microsoft.Azure.KeyVault.Models;
using Microsoft.Azure.Services.AppAuthentication;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Table;

namespace magoutboundsftpfa
{
    public static class Function1
    {
        private const string Host = "ftp.medadvgrp.com";
        private const string UserName = "sftptest";
        private const string Password = "upload!test.99";

        public static CloudStorageAccount GetAuthenticatedCloudStorageAccount(string readWrite, KeyVaultClient kv = null)
        {
            var storageAccountName = ConfigurationManager.AppSettings["StorageAccountName"];
            if (kv == null)
            {
                var azureTokenProvider = new AzureServiceTokenProvider();
                kv = new KeyVaultClient(new KeyVaultClient.AuthenticationCallback(azureTokenProvider.KeyVaultTokenCallback));
            }

            // Get a SAS token for our storage from Key Vault

            // Create new storage credentials using the SAS token.
            var accountSasCredential = new StorageCredentials(GetKeyVaultValueFromSecret(kv, $"{storageAccountName}-{readWrite}").Value);

            // Use the storage credentials and the Blob storage endpoint to create a new Blob service client.
            var accountWithSas = new CloudStorageAccount(accountSasCredential, new Uri($"https://{storageAccountName}.blob.core.windows.net/"), null, new Uri($"https://{storageAccountName}.table.core.windows.net/"), null);

            return accountWithSas;
        }

        public static SecretBundle GetKeyVaultValueFromSecret(KeyVaultClient kv, string secretIdentifier)
        {
            var keyVaultName = ConfigurationManager.AppSettings["KeyVaultName"];
            var sasToken = (kv.GetSecretAsync($"https://{keyVaultName}.vault.azure.net/secrets/{secretIdentifier}"));

            Task.WaitAll(sasToken);

            return sasToken.Result;
        }

        [FunctionName("Function1")]
        public static void Run([BlobTrigger("mag-outbound-sftp-blob-container/{CarrierID}/{POID}/{DestinationFolder}/{FileName}", Connection = "AzureWebJobsDashboard")]Stream myBlob, string CarrierID, string POID, string DestinationFolder, string FileName, TraceWriter log)
        {
            string status = "";

            // Create KeyVaultClient with vault credentials
            AzureServiceTokenProvider azureTokenProvider = new AzureServiceTokenProvider();
            KeyVaultClient kv = new KeyVaultClient(new KeyVaultClient.AuthenticationCallback(azureTokenProvider.KeyVaultTokenCallback));

            CloudStorageAccount cloudStorageAccount = GetAuthenticatedCloudStorageAccount(ConfigurationManager.AppSettings["FullAccessSecretSuffix"], kv);
            CloudBlobClient cloudBlobClient = cloudStorageAccount.CreateCloudBlobClient();
            CloudTableClient cloudTableClient = cloudStorageAccount.CreateCloudTableClient();

            // Use the blobClientWithSas
            CloudBlobContainer cloudBlobContainer = cloudBlobClient.GetContainerReference("mag-outbound-sftp-blob-container");
            CloudBlockBlob cloudBlockBlob = cloudBlobContainer.GetBlockBlobReference(FileName);


            CloudTable cloudTable = cloudTableClient.GetTableReference("Connections");
            TableOperation tableOperation = TableOperation.Retrieve<SFTPConnectionEntity>(CarrierID, POID, null);

            TableResult tableResult = cloudTable.Execute(tableOperation);

            SFTPConnectionEntity sFTPConnectionEntity = (SFTPConnectionEntity)tableResult.Result;
            try
            {
                if (cloudBlockBlob.Exists())
                {
                    //Sftp.UploadSFTPFile(GetKeyVaultValueFromSecret(kv, $"{sFTPConnectionEntity.KeyVaultSecret}-URL").Value, GetKeyVaultValueFromSecret(kv, $"{sFTPConnectionEntity.KeyVaultSecret}-USERNAME").Value, GetKeyVaultValueFromSecret(kv, $"{sFTPConnectionEntity.KeyVaultSecret}-PASSWORD").Value, cloudBlockBlob, DestinationFolder, Int32.Parse(GetKeyVaultValueFromSecret(kv, $"{sFTPConnectionEntity.KeyVaultSecret}-PORT").Value), log);
                }

                status = "Success";
            }
            catch (Exception e)
            {
                status = "Failed";
            }

            cloudTable = cloudTableClient.GetTableReference("Log");
            LogEntity logEntity = new LogEntity(CarrierID, POID)
            {
                Status = status,
                DestinationFolder = DestinationFolder,
                FileName = FileName,
                FileSize = myBlob.Length.ToString()
            };

            tableOperation = TableOperation.Insert(logEntity);
            cloudTable.Execute(tableOperation);
        }
    }

    internal class LogEntity : TableEntity
    {
        public LogEntity(string CarrierID, string POID)
        {
            this.PartitionKey = CarrierID;
            this.RowKey = POID;
        }

        public string Status { get; set; }
        public string FileName { get; set; }
        public string FileSize { get; set; }
        public string DestinationFolder { get; set; }


    }

    internal class SFTPConnectionEntity : TableEntity
    {
        public SFTPConnectionEntity(string CarrierID, string POID)
        {
            this.PartitionKey = CarrierID;
            this.RowKey = POID;
        }

        public SFTPConnectionEntity() { }

        public string Protocol { get; set; }

        public string KeyVaultSecret { get; set; }
    }
}
